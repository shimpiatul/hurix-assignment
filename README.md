# Hurix Dashboard #

Angular application to draw multi line chart with D3

### Links ###

* [Live Application]( http://hurix-dashboard.com.s3-website.ap-south-1.amazonaws.com){:target="_blank"}
* [Code Walkthrough Video]( https://youtu.be/SbtoH4N5lT4){:target="_blank"}

### Technology Stack ###

* Angular 10
* D3 / Angular Material

### How to Run ? ###

* clone the repository
* Install dependencies (npm install) from root
* Run the application (npm run start)
Thats it. This will open the browser and launch the application

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to ? ###

* Atul Shimpi (shimpiatul@hotmail.com)